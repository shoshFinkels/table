--create database testTask
use testTask
create table Users_tbl(
idUser int identity(1,1) primary key,
nameUser varchar(15),
passwordUser varchar(15)
)
create table Tasks_tbl(
id int identity(1,1) primary key,
idUser int references  Users_tbl(idUser) not null,
dateCreate DATEtime not null,
title nvarchar(50) not null,
DescriptionTask nvarchar(300),
dateComplete DATEtime not null,
statusTask bit
)

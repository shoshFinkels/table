import { Users } from 'src/model/classes/Users';
import { UserService } from 'src/model/services/user.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  @Output() loginOK: EventEmitter<any> = new EventEmitter<any>();
  @Input() newUser: boolean;
  @Input() numUserConected: number;
  constructor(private userService: UserService) { }
  user: Users
  numUser: number;
  // open: boolean = false
  TempPassword: string;

  ngOnInit() {
    this.user = new Users(0, "", "");
    this.numUser = null;
  }
  // toConectNew()
  // {
  //   this.newUser=true;
  //   this.open=true;
  // }
  onSubmit() {
    // בדיקה אם משתמש חדש להוסיף בדיקת אימות נוספת ולשלוח לפונקצית הוספה
    if (this.newUser == true && this.TempPassword == this.user.passwordUser) {
      this.userService.toAdd(this.user).subscribe(
        data => { this.numUser = data, this.checkLog() },
        err => { alert(err.message) });
    }
    // שולח לפונקצית התחברות ובדיקה אם השם משתמש מתאים לסיסמא
    else {
      this.userService.toCheckLogin(this.user).subscribe(
        data => { this.numUser = data, this.checkLog() },
        err => { alert(err.message) });
    }
  }
  // אם הצליח להתחבר ע"י רישום או התחברות שולח את הנתונים לאבא
  checkLog() {
    debugger
    if (this.numUser > 0) {
      this.user.idUser = this.numUser;
      this.loginOK.emit(this.user);
    }
  }
}


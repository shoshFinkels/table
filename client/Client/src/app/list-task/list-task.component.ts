import { Component, OnInit, Input } from '@angular/core';
import { Task } from 'src/model/classes/Task';
import { TasksService } from 'src/model/services/tasks.service';

@Component({
  selector: 'app-list-task',
  templateUrl: './list-task.component.html',
  styleUrls: ['./list-task.component.css']
})
export class ListTaskComponent implements OnInit {
  @Input() nowUser: number;
  @Input() nowNameUser: string;
  constructor(private taskService: TasksService) { }
  openAdd: boolean = false;
  tempTask: Task;
  listTask: Array<Task>
  editTask: boolean = false;
  ngOnInit() {
    this.tempTask = new Task(0, this.nowUser, null, "", "", null, false);
    this.viewTasks();
  }
  // הצגת כל המשימות למשתמש
  viewTasks() {
    this.taskService.toGetAllTasks(this.nowUser)
      .subscribe(data => { this.listTask = data; },
        err => { alert(err.massage); });

  }
  openAddLine() {
    this.openAdd = true;
    this.tempTask = new Task(0, this.nowUser, new Date(), "", "", null, false);
  }
  toEdit(t: Task) {
    this.editTask = true;
    this.tempTask = t;
  }
  toDelete(taskID: number) {
    this.taskService.deleteTask(taskID).subscribe(
      data => { this.viewTasks() },
      err => { alert(err.message) });
  }
  ToSave(t: Task) {
    // בדיקה האם המשימה חדשה או נערכה 
    if (t.id != 0) {
      this.taskService.edit(t).subscribe(
        data => { this.viewTasks() },
        err => { alert(err.message) });
    }
    else {
      this.taskService.add(t).subscribe(
        data => { this.viewTasks() },
        err => { alert(err.message) });
    }
    this.toCancel();
  }
  // איפוס הנתונים השימושים
  toCancel() {
    debugger
    this.openAdd = false;
    this.editTask = false;
    this.tempTask.id = 0;
    this.viewTasks();
  }
}

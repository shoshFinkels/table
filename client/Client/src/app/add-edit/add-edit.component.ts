import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from 'src/model/classes/Task';
import { TasksService } from 'src/model/services/tasks.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.css']
})
export class AddEditComponent implements OnInit {
  @Input() taskToAddOrEdit:Task;
  @Output() Cancel:EventEmitter<any>=new EventEmitter<any>();
  @Output() Save:EventEmitter<any>=new EventEmitter<any>();
  constructor() { }
  copyTask: Task;
  ngOnInit() {
    this.copyTask = { ...this.taskToAddOrEdit };
  }
  toSave(t:Task)
  {
    debugger;
    this.Save.emit(t);
    this.Cancel.emit();
  }
}

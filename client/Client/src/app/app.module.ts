import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UserService } from 'src/model/services/user.service';
import { ListTaskComponent } from './list-task/list-task.component';
import { AddEditComponent } from './add-edit/add-edit.component';
import { TasksService } from 'src/model/services/tasks.service';
import { HttpClientModule } from '@angular/common/http'; 

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListTaskComponent,
    AddEditComponent
  ],
  imports: [
    BrowserModule,FormsModule,HttpClientModule
  ],
  providers: [UserService,TasksService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component } from '@angular/core';
import { Users } from 'src/model/classes/Users';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Client';
  viewTasks: boolean = false;
  numUserConected: number = 0;
  nameUserConected: string = "אורח"
  loginNewUser: boolean;
  openLogin: boolean;
  // כאשר מצליח להתחבר שומר את פרטי המשתמש כדי לפתוח את המשימות שלו
  login(u: Users) {
    this.numUserConected = u.idUser;
    this.nameUserConected = u.nameUser;
    this.viewTasks = true;
  }
  toExit() {
    this.viewTasks=false;
    this.openLogin = false;
    this.loginNewUser = false;
    this.numUserConected = 0;
    this.nameUserConected = "אורח"
  }
  // כאשר לוחצים על הרשמה
  loginNew() {
    debugger;
    this.loginNewUser = true;
    this.loginOpen();
  }
  loginOpen() {
    this.openLogin = true;
    this.viewTasks = false;
  }
}

export class Task
{
    constructor(
        public id:number,
        public idUser:number,
        public dateCreate:Date, 
        public title:string, 
        public DescriptionTask:string, 
        public dateComplete:Date, 
        public statusTask:boolean ){
    }
}
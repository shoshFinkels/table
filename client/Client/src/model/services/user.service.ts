import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Users } from '../classes/Users';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  basicUrl: string = "http://localhost:63026/"
  constructor(private http: HttpClient) { }

  toAdd(u: Users) {
    debugger;
    return this.http.post<number>(this.basicUrl + "api/usersController/addUser", u);
  }
  toCheckLogin(u: Users): Observable<number> {
    debugger;
    return this.http.post<number>(this.basicUrl + "api/usersController/checkUser", u);
  }
}

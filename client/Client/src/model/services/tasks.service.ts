import { Injectable } from '@angular/core';
import { Task } from '../classes/Task';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TasksService {
  basicUrl: string = "http://localhost:63026/api/tasksController/"
  constructor(private http: HttpClient) { }
  edit(t: Task) {
    return this.http.post<boolean>(this.basicUrl + "editTask", t);
  }
  add(t: Task) {
    debugger;
    return this.http.post<Task>(this.basicUrl + "addNeW", t);
  }
  deleteTask(tId: number) {
    return this.http.delete(this.basicUrl + "deleteTask/" + tId)
  }
  toGetAllTasks(userConected: number):Observable<Array<Task>>
  {
    return this.http.get<Array<Task>>(this.basicUrl + "getAllTaskToUser/" + userConected);
  }
}

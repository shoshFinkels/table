USE [testTask]
GO
/****** Object:  Table [dbo].[Tasks_tbl]    Script Date: 23/11/2019 22:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasks_tbl](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUser] [int] NOT NULL,
	[dateCreate] [datetime] NOT NULL,
	[title] [nvarchar](50) NOT NULL,
	[DescriptionTask] [nvarchar](300) NULL,
	[dateComplete] [datetime] NOT NULL,
	[statusTask] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users_tbl]    Script Date: 23/11/2019 22:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users_tbl](
	[idUser] [int] IDENTITY(1,1) NOT NULL,
	[nameUser] [varchar](15) NULL,
	[passwordUser] [varchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[idUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tasks_tbl]  WITH CHECK ADD FOREIGN KEY([idUser])
REFERENCES [dbo].[Users_tbl] ([idUser])
GO
